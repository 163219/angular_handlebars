var express = require("express");
var app = express();

var handlebars = require("express-handlebars");
app.engine("handlebars", handlebars({ defaultLayout: "main"}));
app.set("view engine", "handlebars");

app.get("/", function(req, res) {
    //Initialize Angular model with preset data
    res.render("index", {
        initCart: JSON.stringify(['apple', 'orange', 'pear', 'grapes'])
    });
});

app.use("/bower_components", express.static(__dirname + "/bower_components"));
app.use("/public", express.static(__dirname + "/public"));

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);
app.listen(app.get("port"), function(req, res) {
    console.info("Application started on port %d", app.get("port"));
});

